# V1 pelicula

html de pelicula más css3

1. font-size: 320%;        --- esto define al titulo con un tamaño de letra del 420% de la pantalla
2. text-align: center;     --- alinea el texto en el centro del dv padre
3. color: #ff9bfb;         --- define el color del texto en el esccrito
4. font-family: Mukta; /* fuente de google */  --- define la fuente del texto en una de google fonts
5. display:flex;           --- indicamos al div que tenga las características del flexbox
6. flex-direction: row;    --- indicamos que la direccion de posicionamiento de los elementos del interior del div sea en dirección horizontal 7. width: 50%;             --- el ancho de este div es de 50% del ancho del div padre
8. padding-right: 20px;    --- los elementos de dentro de este div tendran un margen interior de 20px
9. text-align: justify;    --- el texto se alineará de forma que quede centrado de manera que el texto se justifica para que utilice todo el espacio que tiene
10. height: 250px;         --- define la altura de un div en 250px
11. margin-left: 30px;     --- define el margen de la izquierda solamente en 30px
12. text-decoration: none; --- no hya decoración del texto, si lo ponemos en un link <a> quitará el subrallado
13. border: 3px solid #fcc9e1;  --- declaramos al div que tenga una linea de borde a su alrededor de 3px de ancho, solido(linea continua) y el color especificado
14. border-radius: 10px;   --- el borde en las esquinas se le aplica una curva de 10px de radio
15. display: inline-block; --- los divs de su interior se colocaran en fila horizontal de forma que a ellos les afecte el top,left,... si lo definimos
16. background: #ffffff;   --- define el fondo del div con el color blanco en este caso
17. display: none;         --- hacemos que por definido no aparezca en pantalla en el dropdown
28. body{...}              --- decimos que el elemento body del html tendrá las caractersiticas que definamos dentro de {...}
29. .titol{...}            --- en este caso definimos que los elementos del html con class="titol" tendrán los elementos definidos dentro de {...}
23. .dropdown:hover .drop{...}  --- dentro de la clase dropdown, cuando pasemos el raton por encima(:hover), en los elementos .drop se les cambiará las caracteristicas definidas {...} 
24.
background: -moz-linear-gradient(left, #ffffff 0%, #ffa5e7 100%);
background: -webkit-linear-gradient(left, #ffffff 0%,#ffa5e7 100%);
background: linear-gradient(to right, #ffffff 0%,#ffa5e7 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffa5e7'GradientType=1 ); --- esto lo que hace es de fondo de div, añadir un color degradado de forma que en este caso ira de izquierda(blanco) a (rosa)
25. flex-direction: column; --- define que los div de su interior se alinearán en dirección vertical.